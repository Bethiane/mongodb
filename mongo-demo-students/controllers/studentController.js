const admin = require('../middlewares/admin')
const express = require('express');
const mongoose = require('mongoose');

var router = express.Router();
const Student = mongoose.model('Student');
const School = mongoose.model('School');

router.post('/',admin,(req,res)=>{
    insertIntoMongoDB(req,res);
});

router.put('/',admin,(req,res)=>{
    updateIntoMongoDB(req,res);
});

function insertIntoMongoDB(req,res){
    let student = new Student();
    student.studName = req.body.studName;
    student.schoolId = req.body.schoolId;
    student.studEmail = req.body.studEmail;
    student.studGender = req.body.studGender;
    student.studAge = req.body.studAge;
    student.studSector = req.body.studSector;
    student.save()
        .then(school => res.send(school))
        .catch(err => res.send(err).status(400));
}

function updateIntoMongoDB(req,res){
    Student.findOneAndUpdate({_id:req.body._id},
        req.body, {new: true})
        .then(student => res.send(student))
        .catch(err => res.send(err).status(400));
}

router.get('/',admin,(req,res)=>{
    Student.find()
    .then(student => res.send(student))
    .catch(err => res.send(err).status(404));
});

router.get('/:schoolId',admin,(req,res)=>{
    Student.find({schoolId:req.params.schoolId})
    .then(student => res.send(student))
    .catch(err => res.send(err).status(404));
});

router.get('/totalNumber/:schoolId',admin,(req,res)=>{
    Student.find().countDocuments({schoolId:req.params.schoolId})
    .then(student => res.send(`There are ${student} student(s) in this school`))
    .catch(err => res.send(err).status(404));
});
router.get('/whatGender/:gender',admin,(req,res)=>{
    Student.find().countDocuments({studGender:req.params.gender})
    .then(student => res.send(`There are ${student} student(s) `))
    .catch(err => res.send(err).status(404));
});

router.get('/totalNumberInSectorOfTheirLocation/:sector',async(req,res)=>{
    Student.find().countDocuments({studSector:req.params.sector})
    .then(student=>res.send(`There are ${Student} student in this sector`))
    .catch(err=>res.send(err).status(404));
})

router.get('/inThatSector/:sector',(req,res)=>{
    Student.find({studSector:req.params.sector})
        .then(student => res.send(student))
        .catch(err => res.send(err).status(404));
});

router.get('/totalNumberInSectorOfSchool/:sector',async (req,res)=>{
    let i=0;
    let count;
    var total=0;
    let schools = await School.find({schoolSector: req.params.sector});
    try{
        for(i=0;i<schools.length;i++){
            count = await Student.find({schoolId:schools[i]._id}).countDocuments()
               console.log(`Total students per sector ${count}`)
               total +=count;
       }
       res.send(`Total students per sector =${total}`).status(200)
    }catch(err){
        console.log(err);
    }
});


router.get('/gender/:gender',(req,res)=>{
    Student.find({gender:req.params.studGender})
        .then(student => res.send(student))
        .catch(err => res.send(err).status(404));
});

router.delete('/:id',admin,(req,res)=>{
    findByIdAndRemove(req.params.id)
    .then(student => res.send(student))
    .catch(err => res.send(err).status(404));
});

module.exports = router;
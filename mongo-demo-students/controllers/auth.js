const hashPassword = require('../utils/hash')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const Joi = require('joi')
const {User} = require('../model/user.model')
const express = require('express')
const config = require('config')
const _ = require('lodash')

var router = express.Router();

router.post('/jwt',async(req,res)=>{
    const {error} = validate(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    let user = await User.findOne({email: req.body.email})
    if(!user) return res.send('Invalid email or password ...').status(400)

    const validPassword = await bcrypt.compare(req.body.email,user.password)
    if(!user) return res.send('Invalid email or password ...').status(400)

    return res.send(user.generateAuthToken())
});

function validate(req){
    const schema = {
        email: Joi.string().min(8).max(30).required().email(),
        password: Joi.string().min(3).max(15).required()
    }
    return Joi.validate(req,schema)
}

module.exports = router;
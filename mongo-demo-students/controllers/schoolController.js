const admin = require('../middlewares/admin');
const express = require('express');
const mongoose = require('mongoose');

var router = express.Router();
const School = mongoose.model('School');

router.post('/',admin,(req,res)=>{
    insertIntoMongoDB(req,res);
});

router.put('/',admin,(req,res)=>{
    updateIntoMongoDB(req,res);
});

function insertIntoMongoDB(req,res){
    let school = new School();
    school.schoolName = req.body.schoolName;
    school.schoolDistrict = req.body.schoolDistrict;
    school.schoolSector = req.body.schoolSector;
    school.schoolEmail = req.body.schoolEmail;
    school.save()
        .then(schoolSaved => res.send(schoolSaved).status(201))
        .catch(err => res.send(err).status(400));
}

function updateIntoMongoDB(req,res){
    School.findOneAndUpdate({_id: req.body._id},
        req.body, {new: true })
        .then(school => res.send(school))
        .catch(err => res.send(err).status(400));
}

router.get('/', admin,(req,res)=>{
    School.find()
        .then(schools => res.send(schools))
        .catch(err => res.send(err).status(404));
});

router.get('/id',admin,(req,res)=>{
    School.findById(req.params.id)
    .then(school=>res.send(school))
    .catch(err=>res.send(err).status(404));
});

router.get('/inDistrict/:district',admin,(req,res)=>{
    School.find({schoolDistrict:req.params.district})
        .then(school => res.send(school))
        .catch(err => res.send(err).status(404));
});
    

router.get('/inSector/:sector',admin,(req,res)=>{
    School.find({schoolSector:req.params.sector})
        .then(school => res.send(school))
        .catch(err => res.send(err).status(404));
})

router.delete('/:id',admin,(req,res)=>{
    School.findByIdAndRemove(req.params.id)
        .then(school => res.send(school))
        .catch(err => res.send(err).status(404));
});

function displayCount(schoolEmail){
    School.find({schoolAbbrev:schoolEmail}).countDocuments()
        .then(count => console.log({total:count}))
        .catch(err => console.error(err));
}
module.exports = router;
const mongoose = require('mongoose');

var schoolSchema = new mongoose.Schema({
    schoolName:{
        type: String,
        min: 7,
        required: 'Fill this field!'
    },
    schoolDistrict:{
        type: String,
        min:  3,
        required: 'Fill this field!'
    },
    schoolSector:{
        type: String,
        min: 6,
        max: 100,
        required: 'Fill this field!'
    },
    schoolEmail:{
        type: String,
        unique: true,
        maxlength: 255,
        minlength: 3,
        required: 'Fill this field!'
    }
});

mongoose.model('School', schoolSchema);
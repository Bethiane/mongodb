const mongoose = require('mongoose');

var studentSchema = new mongoose.Schema({
    studName:{
        type: String,
        min: 5,
        max: 15,
        required: 'Fill this field!'
    },
  schoolId:{
        type: String,
        max: 200,
        required: 'Fill this field!'
    },
    studEmail:{
        type: String,
        unique: true,
        maxlength: 255,
        minlength: 3,
        required: 'Fill this field!'
    },
    studGender:{
        type: String,
        min: 4,
        required: 'Fill this field!'
    },
    studAge:{
        type: Number,
        min: 2,
        max: 25,
        required: 'Fill this field!'
    },
    studSector: {
        type: String,
        min: 2,
        required: 'Fill this field!'
    }
    
});

mongoose.model('Student', studentSchema);
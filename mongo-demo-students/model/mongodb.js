const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/school-demo',{
    useNewUrlParser: true,
    useUnifiedTopology: true
})
.then(() => console.log('connected to mongodb successfully ...'))
.catch(err => console.log('failed to connect to mongodb',err));

require('./school.model');
require('./student.model');
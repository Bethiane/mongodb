require('./model/mongodb')
const schoolController = require('./controllers/schoolController');
const studentController = require('./controllers/studentController');
const userController = require('./controllers/userController')
const auth = require('./controllers/auth')
const authMiddleware = require('./middlewares/auth')
const config = require('config')
const express = require('express');

var app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

if(!config.get("jwtPrivateKey")){
    console.log('JWT private key is not defined')
    process.exit(1)
}

app.get('/',(req,res)=>{
    res.send('Welcome to our app ...');
});

app.use('/api/schools',authMiddleware, schoolController);
app.use('/api/students',authMiddleware, studentController);
app.use('/api/users',userController);
app.use('/api/auth',auth)

const port = process.env.PORT || 8000;
app.listen(port, () => console.log(`Listening to the port ${port}`));
const hashPassword = require('../utils/hash')
const _=require('lodash')
const express=require('express');
const {User,validate} =require('../model/user.model')


var router = express.Router();

router.get('/',async(req,res)=>{
    const users = await User.find().sort({name:1});
return res.send(users)
});

router.get('/:email',async(req,res)=>{
    const users =await User.find({email: req.params.email});

});

router.post('/',async(req,res)=>{
    const{ error } =validate(req.body)
    if(error) return res.send(error.details[0].message).status(400)
    let user =await User.findOne({email:req.body.email})
    if(user) return res.send('already registered').status(400)
    user = new User(_.pick(req.body,['name','email','password']))
    const hashed =await hashPassword(user.password)
    user.password = hashed
    if(req.body.isAdmin="true"){
        user.isAdmin=true;
    }
    await user.save()

    return res.send(_.pick(user,['_id','name','email'])).status(201)
});

module.exports =router;
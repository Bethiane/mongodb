const admin = require('../middlewares/admin')
const express = require('express');
const mongoose =require('mongoose');

var router = express.Router();
const Book = mongoose.model('Book');

router.post('/',(req,res)=>{
    insertIntoMongoDB(req, res);
});

router.put('/',(req,res)=>{
    updateIntoMongoDB(req,res);
});

function insertIntoMongoDB(req,res){
    let book =new Book();
    book.bookName =req.body.bookName;
    book.bookId = req.body.bookId;
    book.ISBN = req.body.ISBN;
    book.pages = req.body.pages;
    book.bookTitle = req.body.bookTitle;
    book.save()
       .then(bookSaved=>res.send(bookSaved).status(201))
       .catch(err=>res.send(err).status(400));

function updateIntoMongoDB(req,res){
    Book.findOneAndUpdate({_id:req.body._id},
        req.body, {new:true})
        .then(book=>res.send(book))
        .catch(err=>res.send(err).status(400));
}

router.get('/',(req,res)=>{
    Book.find()
    .then(book=>res.send(books))
    .catch(err=>res.send(err).status(404))
});


router.get('/id',(req,res)=>{
    Book.findById(req.params.id)
    .then(book=>res.send(book))
    .catch(err=>res.send(err).status(404));

});

router.delete('/:id',(req,res)=>{
    Book.findByIdAndRemove(req.params.id)
    .then(book=>res.send(book))
    .catch(err=>res.send(err).status(404));
});

}
module.exports=router;
const mongoose = require('mongoose');
const Joi = require('joi')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const config = require('config')

var userSchema=new mongoose.Schema({
name:{
    type:String,
    required:true,
    maxlength:255,
    minlength:3
  },

    email:{
        type:String,
        required:true,
        maxlength:255,
        minlength:5

    },

    password:{
        type:String,
        required:true,
        maxlength:255,
        minlength:3
    },

    isAdmin:{
        type:Boolean,
        default:false,
        required:true
    }

});

userSchema.methods.generateAuthToken=function(){
    const token =jwt.sign({_id:this._id,name:this.name,email:this.email,isAdmin:this.email}
        ,config.get('jwtPrivateKey'))
        return token
}

const User = mongoose.model('User',userSchema);

function validateUser(user){
    const schema = {
        name:Joi.string().max(255).min(3).required(),
        email:Joi.string().max(255).min(3).required(),
        password:Joi.string().max(255).min(3).required(),
        isAdmin:Joi.required()
}

return Joi.validate(user,schema)
}

module.exports.User = User;
module.exports.validate = validateUser;
const mongoose=require('mongoose')


//Attributes of the Author object
 var authorSchema=new mongoose.model({

    authorName: {
        type:String,
        required:'the name is required'
    },

    authorEmail: {
        type:String,
        required:'the email is required'
    },

    location: {
        type:String,
        required:'the locaation is required'
    },

    telephone: {
        type:Number,
        required:'the telephone is required'
    }
});

mongoose.model('Author',authorSchema);
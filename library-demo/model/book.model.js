const mongoose= require('mongoose');

var bookSchema =new mongoose.Schema({
    bookName: {
        type:String,
        required:'the name is required'
    },
   
    bookId:{
        type:Number,
        required:'the id is required'
    },

    ISBN:{
        type:String,
        required:'the ISBN is required'
    },
    
    pages:{
        type:Number,
        required:'the pages are required'
    },

    bookTitle:{
        type:String,
        required:'the title is needed'
    },
});

mongoose.model('Book',bookSchema);
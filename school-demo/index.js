require('./model/mongodb')
const schoolController = require('./controllers/schoolController');
const studentController = require('./controllers/studentController');
const userController = require('./controllers/userController');
const auth = require('./controllers/auth')
const middlewareAuth = require('./middlewares/auth');
const config = require('config')

const express = require('express');

var app = express();
const bodyparser = require('body-parser');

app.use(bodyparser.urlencoded({extended:true}));
app.use(bodyparser.json());

if(!config.get("jwtPrivateKey")){
    console.log('JWT PRIVATE KEY IS NOT DEFINED')
    process.exit(1)
}

app.get('/',(req,res)=>{
    res.send('Warm welcome to my application');
});

app.use('/api/schools',middlewareAuth,schoolController);
app.use('/api/students',middlewareAuth,studentController);
app.use('/api/users',userController)
app.use('/api/auth',auth)


const port = process.env.PORT || 7000;
app.listen(port, ()=>console.log(`Listening on port ${port}..`));
const brcypt = require('bcrypt')
async function hashPassword(password){
    const  salt = await brcypt.genSalt(10)
    const hashed =await brcypt.hash(password,salt)
    //console.log(salt)
    //console.log(hashed)
    return hashed
}
hashPassword('12345')
module.exports =hashPassword
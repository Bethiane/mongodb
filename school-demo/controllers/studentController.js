const admin = require('../middlewares/admin')
const schoolAdmin = require('../middlewares/schoolAdmin')
const express = require('express');
const mongoose = require('mongoose');

var router = express.Router();
const Student = mongoose.model('Student');

router.post('/',admin,schoolAdmin,(req,res)=>{
    insertIntoMongoDB(req, res);
});

router.put('/',admin,schoolAdmin,(req,res)=>{
    updateIntoMongoDB(req,res);
});

function insertIntoMongoDB(req,res){
    let student = new Student();
    student.studentName=req.body.studentName;
    student.schoolId=req.body.schoolId;
    student.studentGender=req.body.studentGender;
    student.studentAge=req.body.studentAge;
    student.studentEmail=req.body.studentEmail;
    student.save()
       .then(studentSaved=>res.send(studentSaved).status(201))
       .catch(err=>res.send(err).status(400));
}
function updateIntoMongoDB(req,res){
    Student.findOneAndUpdate({_id:req.body._id},req.body,{new:true})
    .then(student=>res.send(studentSaved).status(201))
    .catch(err=>res.send(err).status(400));
}

router.get('/',(req,res)=>{
    Student.find()
    .then(student=>res.send(students))
    .catch(err=>res.send(err).status(404))
});

router.get('/id',(req,res)=>{
    Student.findById(req.params.id)
    .then(book=>res.send(student))
    .catch(err=>res.send(err).status(404));
});

router.delete('/id:',admin,schoolAdmin,(req,res)=>{
    Student.findByIdAndRemove(req.params.id)
    .then(student=>res.send(student))
    .catch(err=>res.send(err).status(404));
});


router.get('/countStuByG/:gender',(req,res)=>{
    Student.find({gender:req.params.gender}).countDocuments()
     .then(count =>res.send(count.toString()))
     .catch(err => res.send(err).status(400));
           } )
/* async function studentfromsector(sectorname){
    let i = 0;
    let schools = await School.find({sector: sectorname});
      try{
          for(i = 0; i < schools.length; i++)
            students = await Student.find({schoolId: schools[i]._id}).countDocuments();
          console.log({studentsFromSector : students})
        }
        catch (err){
            console.error(err);
        }
   }
    studentfromsector('mukamira'); */
    
 
module.exports=router;
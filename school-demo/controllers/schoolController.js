const admin = require('../middlewares/admin')
const express = require('express');
const mongoose = require('mongoose');

var router = express.Router();
const School = mongoose.model('School');


router.post('/',(req,res)=>{
    insertIntoMongoDB(req, res);
});

router.put('/',(req,res)=>{
    updateIntoMongoDB(req,res);
});


function insertIntoMongoDB(req,res){
    let school = new School();
    school.schoolName=req.body.schoolName;
    school.district=req.body.district;
    school.sector=req.body.sector;
    school.save()
    .then(schoolSaved=>res.send(schoolSaved).status(201))
    .catch(err=>res.send(err).status(400));
}

    function updateIntoMongoDB(req,res){
        School.findOneAndUpdate({_id:req.body._id},req.body,{new:true})
        .then(schoolSaved=>res.send(schoolSaved).status(201))
        .catch(err=>res.send(err).status(400));
    }

    router.get('/',(req,res)=>{
        School.find()
        .then(school=>res.send(schools))
        .catch(err=>res.send(err).status(400));
    });

    router.get('/id',(req,res)=>{
        School.findById(req.params.id)
        .then(school=>res.send(school))
        .catch(err=>res.send(err).status(404));
    });

    router.delete('/id:',(req,res)=>{
        School.findByIdAndRemove(req.params.id)
        .then(school=>res.send(school))
        .catch(err=>res.send(err).status(404));
    });
    router.get('/sector/:sector',(req,res)=>{
        School.find({sector:req.params.sector})
        .then(school=>res.send(school))
        .catch(err=>res.send(err.details[0].message))

    })

module.exports=router;
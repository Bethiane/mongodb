const mongoose=require('mongoose')

mongoose.connect('mongodb://localhost/mongo-demo',{
    useNewUrlParser:true,
    useUnifiedTopology:true
})

.then(()=>console.log('Successful connect to mongodb'))
.catch(err=>console.log('Failed to connect to db',err));

require('./school.model');
require('./student.model');
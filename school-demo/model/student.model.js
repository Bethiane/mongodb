const mongoose = require('mongoose')

var studentSchema=new mongoose.Schema({

    studentName:{
        type:String,
        required:'fill it'
    },
  /*   schoolId:{
        type:Object,
        required:'fill this field'
    }, */
    studentGender:{
        type:String,
        required:'fill the field'
    },
    studentAge:{
        type:Number,
        required:'fill it'
    },
    studentEmail:{
        unique:true,
        type:String,
        required:'fill it'
    }
});

mongoose.model('Student',studentSchema);
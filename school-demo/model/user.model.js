const mongoose = require('mongoose');
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const Joi = require('joi')
const config = require('config')

var userSchema=new mongoose.Schema({

    name:{
        type:String,
        required:true,
        maxlength:250,
        minlength:3
    },

    email:{
        type:String,
        unique:true,
        required:true,
        maxlength:255,
        minlength:3
    },

    password:{
        type:String,
        required:true,
        maxlength:255,
        minlength:3
    },

  /*   schoolId:{
        type:String,
        required:true,
        maxlength:250,
        minlength:3
    },  */

    forAdmin:{
        type:Boolean,
        required:true,
        default:false,
    }
});

userSchema.methods.generateAuthToken=function(){
    const token=jwt.sign({_id:this._id,name:this.name,email:this.email, schoolId:this.schoolId,forAdmin:this.forAdmin},config.get('jwtPrivateKey'))
    return token
}

const User = mongoose.model('Userz',userSchema);

function validateUser(user){
 const schema={
     name:Joi.string().max(255).min(3).required(),
     email:Joi.string().max(255).min(3).required(),
     password:Joi.string().max(255).min(3).required(),
   /*   schoolId:Joi.string().max(255).min(3).required(), */
     forAdmin:Joi.required()
 }

 
 return Joi.validate(user,schema)
}

module.exports.User=User;
module.exports.validate=validateUser;
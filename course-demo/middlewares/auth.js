const config = require('config')//for private key
const jwt  =  require('jsonwebtoken')//generated for  knowing whom he/she is

function auth(req,res,next){
    const token = req.header('x-auth-token')
    if(!token) return res.send('token missing..').status(401)
    try {
        const decoded = jwt.verify(token, config.get('jwtPrivateKey'))
        req.user = decoded
        next()
    } catch (err) {
       return  res.send('invalid token').status(400)
    }
}
module.exports = auth
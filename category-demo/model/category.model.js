const mongoose = require('mongoose');
 
//Attributes of the product object
var categorySchema = new mongoose.Schema({
categoryName: {
type: String,
required: 'This field is required!'
},
description: {
type: String
}
});
 
mongoose.model('category', categorySchema);
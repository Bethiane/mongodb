const express = require('express');
const mongoose = require('mongoose')


//Creating a Router
var router = express.Router();

//Create Category model CLASS

const category = mongoose.model('category')
 
//Router Controller for CREATE request
router.post('/',(req,res)=>{
    insertIntoMongoDB(req,res);
});

 //Router Controller for UPDATE request
router.put('/',(req,res)=>{
    updateIntoMongoDB(req,res);
})

     
//Creating function to insert data into MongoDB
function insertIntoMongoDB(req,res) {
var ctgr = new category();


ctgr.categoryName = req.body.categoryName;
ctgr.description = req.body.description

ctgr.save()
    .then(categorySaved=> res.send(categorySaved).status(201))
    .catch(err => res.send(err).status(400));
}
 
//Creating a function to update data in MongoDB
function updateIntoMongoDB(req, res) {
category.findOneAndUpdate({ _id: req.body._id },
     req.body, { new: true })
    .then(category => res.send(category))
    .catch(err => res.send(err).status(400));
} 

//Router to retrieve the complete list of available courses

router.get('/', (req,res) => {
category.find()
    .then(category => res.send(category))
    .catch(err => res.send(err).status(404));
});


//Router to update a course using it's ID
router.get('/:id', (req, res) => {
category.findById(req.params.id)
    .then(category => res.send(category))
    .catch(err => res.send(err).status(404));
});
 
//Router Controller for DELETE request
router.delete('/:id', (req, res) => {
category.findByIdAndRemove(req.params.id)
    .then(category => res.send(category))
    .catch(err => res.send(err).status(404));
});

//dispayCount('Stanley')
module.exports = router;